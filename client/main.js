import React from 'react';
import ReactDOM from 'react-dom';
import {Meteor} from 'meteor/meteor';
import {Tracker} from 'meteor/tracker';

import { Players } from '../imports/api/players';
import App from '../imports/ui/App';


/*
* Descripton: Meteor startup
* Author: Gagudeep
*/
Meteor.startup(function() {
  /*
  * Descripton: Tracker for listen change. When changes will happends it will change the data everywhere
  * Author: Gagudeep
  */
  Tracker.autorun(function() {
      let players = Players.find({}, {
        sort: {     // Sorting method by mongo default
          score: -1 // -1 Sort Score Desc Order & 1 Sort Score Asc Order
        }
      }).fetch();
      // Title for app
      let title = 'Gugu Score Keep App';
      let subtitle = 'Created By: Gagudeep';

      ReactDOM.render(<App title={title} players={players} subtitle={subtitle} />, document.querySelector("#app"));
  });

});
