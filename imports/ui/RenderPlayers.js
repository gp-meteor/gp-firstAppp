import React from 'react';
import Player from './Player';

/*
* Descripton: All Player list Component
* Author: Gagudeep
*/
export default class RenderPlayers extends React.Component {
  constructor(props) {
    super(props);

  }

  playersList() {
    if(this.props.players.length > 0) {
      return this.props.players.map((player) => {
        return <Player key={player._id} player={player} />;
      });
    } else {
      return (
        <div className="item">
          <p className="item__message">Add Your First Player Using Below Form.</p>
        </div>
      )
    }
  }

  /*
  * Descripton: All jsx
  * Author: Gagudeep
  */
  render() {
    return (
      <div>
        {this.playersList()}
      </div>
    )
  }
}
