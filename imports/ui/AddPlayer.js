import React from 'react';
import { Players } from '../api/players';

/*
* Descripton: Titlebar Component
* Author: Gagudeep
*/
class AddPlayer extends React.Component {
  constructor(props) {
    super(props);
  }

  /*
  * Descripton: Saving player name from form to db
  * Author: Gagudeep
  */
  handleSubmit(e) {
    e.preventDefault();
    let playerName = e.target.playerName.value;

    if(playerName !== '') {
      e.target.playerName.value = '';

      // Inserting in db
      Players.insert({
        name: playerName,
        score: 0
      });
    }
  }

  /*
  * Descripton: All jsx
  * Author: Gagudeep
  */
  render() {
    return(
      <div className="item">
        <form className="form" onSubmit={this.handleSubmit}>
          <input type="text" className="form__input" name="playerName" placeholder="Player"/>
          <button className="button" type="submit">Add Player</button>
        </form>
      </div>
    );
  }
}

/*
* Descripton: Exporting component for views
* Author: Gagudeep
*/
export default AddPlayer;
