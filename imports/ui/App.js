import React from 'react';

import TitleBar from './/TitleBar';
import RenderPlayers from './RenderPlayers';
import AddPlayer from './AddPlayer';

export default class App extends React.Component {
  render() {
    return (
      <div>
        {/* Titlebar for app */}
        <TitleBar title={this.props.title} subtitle={this.props.subtitle} />
        {/* Viewing all players list here */}
        <div className="wrapper">
          <RenderPlayers players={this.props.players} />
          <AddPlayer />
        </div>
      </div>
    );
  }
};
