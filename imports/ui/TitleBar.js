import React from 'react';

/*
* Descripton: Titlebar Component
* Author: Gagudeep
*/
export default class TitleBar extends React.Component {
  constructor(props) {
    super(props);
  }

  /*
  * Descripton: All jsx
  * Author: Gagudeep
  */
  render() {
    return(
      <div className="title-bar">
        <div className="wrapper">
          <h1>{this.props.title}</h1>
          <h2 className="title-bar__subtitle">{this.props.subtitle}</h2>
        </div>
      </div>
    );
  }
}

// TitleBar.propTypes = {
//   title: React.propTypes.string.isRequired
// };
