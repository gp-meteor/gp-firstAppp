import React from 'react';
import FlipMove from 'react-flip-move';

import { Players } from '../api/players';

class Player extends React.Component {
  constructor(props) {
    super(props);
  }

  playerActions() {
    return (
      <div className="item" key={this.props.player._id}>
        <div className="player">

          <div>
            <h3 className="player__name">{this.props.player.name}</h3>
            <p className="player__stats">
               {this.props.player.score} point(s)
            </p>
          </div>

          <div className="player__action">
            <button className="button button--round" onClick={() => {
                Players.update({_id: this.props.player._id}, {
                  $inc: { score: -1 }
                });
            }}>-</button>
            <button className="button button--round" onClick={() => {
                Players.update({ _id: this.props.player._id }, {
                  $inc: { score: 1 }
                });
            }}>+</button>
            <button className="button button--round" onClick={() => { this.deletePlayer(this.props.player._id); }}>X</button>
          </div>

        </div>
      </div>
    )
  }

  /*
  * Descripton: Delete player from list
  * Author: Gagudeep
  */
  deletePlayer(playerId) {
    Players.remove({
      _id: playerId
    });
  }

  render() {
    return (
      <div>
         <FlipMove maintainContainerHeight={true}>
           {this.playerActions()}
         </FlipMove>
      </div>
    )
  }
}

Player.propTypes = {
  //player: React.propTypes.object.isRequired
}

export default Player;
